﻿using System;
using System.Runtime.InteropServices;

namespace WebView.Native
{
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void WebViewExternalInvokeCallback(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string arg);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void WebViewDispatch(ref WebView w, IntPtr arg);

    public abstract class WebViewLibrary
    {
        public static WebViewLibrary Instance { get; }

        static WebViewLibrary()
        {
            WebViewLibrary library = null;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                library = new WebViewLibraryLinux();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                switch (RuntimeInformation.OSArchitecture)
                {
                    case Architecture.X64:
                        library = new WebViewLibraryWin64();
                        break;

                    case Architecture.X86:
                        library = new WebViewLibraryWin86();
                        break;

                    default:
                        throw new PlatformNotSupportedException("WebView only supports win32, win64 and linux64");
                }
            }

            Instance = library;
        }

        public abstract int Simple(string title, string url, int width, int height, bool resizable);

        public abstract int Init(ref WebView webView);

        public abstract int Loop(ref WebView webView, int blocking);

        public abstract int Eval(ref WebView webView, string js);

        public abstract int InjectCss(ref WebView webView, string css);

        public abstract void SetTitle(ref WebView webView, string title);

        public abstract void SetFullscreen(ref WebView webView, int fullscreen);

        public abstract void SetColor(ref WebView webView, byte r, byte g, byte b, byte a);

        public abstract void Dialog(ref WebView webView, DialogType dialogType, DialogFlags flags, string title,
            string arg, string result, int resultSize);

        public abstract void Dispatch(ref WebView webView, WebViewDispatch callback, IntPtr arg);

        public abstract void Terminate(ref WebView webView);

        public abstract void Exit(ref WebView webView);
    }

    internal class WebViewLibraryWin86 : WebViewLibrary
    {
        private const string LibraryName = "webview_x86";

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview(
            [MarshalAs(UnmanagedType.LPStr)] string title,
            [MarshalAs(UnmanagedType.LPStr)] string url,
            int width, int height, int resizable);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_init(ref WebView w);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_loop(ref WebView w, int blocking);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_eval(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string js);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_inject_css(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string css);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_set_title(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string title);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_set_fullscreen(ref WebView w, int fullscreen);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_set_color(ref WebView w, byte r, byte g, byte b, byte a);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_dialog(ref WebView w, DialogType dlgtype, DialogFlags flags,
            [MarshalAs(UnmanagedType.LPStr)] string title,
            [MarshalAs(UnmanagedType.LPStr)] string arg,
            [MarshalAs(UnmanagedType.LPStr)] string result,
            UIntPtr resultsz);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_dispatch(ref WebView w,
            [MarshalAs(UnmanagedType.FunctionPtr)] WebViewDispatch fn,
            IntPtr arg);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_terminate(ref WebView w);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_exit(ref WebView w);

        public override int Simple(string title, string url, int width, int height, bool resizable)
        {
            return webview(title, url, width, height, resizable ? 1 : 0);
        }

        public override int Init(ref WebView webView)
        {
            return webview_init(ref webView);
        }

        public override int Loop(ref WebView webView, int blocking)
        {
            return webview_loop(ref webView, blocking);
        }

        public override int Eval(ref WebView webView, string js)
        {
            return webview_eval(ref webView, js);
        }

        public override int InjectCss(ref WebView webView, string css)
        {
            return webview_inject_css(ref webView, css);
        }

        public override void SetTitle(ref WebView webView, string title)
        {
            webview_set_title(ref webView, title);
        }

        public override void SetFullscreen(ref WebView webView, int fullscreen)
        {
            webview_set_fullscreen(ref webView, fullscreen);
        }

        public override void SetColor(ref WebView webView, byte r, byte g, byte b, byte a)
        {
            webview_set_color(ref webView, r, g, b, a);
        }

        public override void Dialog(ref WebView webView, DialogType dialogType, DialogFlags flags, string title,
            string arg, string result, int resultSize)
        {
            webview_dialog(ref webView, dialogType, flags, title, arg, result, (UIntPtr)resultSize);
        }

        public override void Dispatch(ref WebView webView, WebViewDispatch callback, IntPtr arg)
        {
            webview_dispatch(ref webView, callback, arg);
        }

        public override void Terminate(ref WebView webView)
        {
            webview_terminate(ref webView);
        }

        public override void Exit(ref WebView webView)
        {
            webview_exit(ref webView);
        }
    }

    internal class WebViewLibraryWin64 : WebViewLibrary
    {
        private const string LibraryName = "webview_x64";

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview(
            [MarshalAs(UnmanagedType.LPStr)] string title,
            [MarshalAs(UnmanagedType.LPStr)] string url,
            int width, int height, int resizable);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_init(ref WebView w);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_loop(ref WebView w, int blocking);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_eval(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string js);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_inject_css(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string css);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_set_title(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string title);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_set_fullscreen(ref WebView w, int fullscreen);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_set_color(ref WebView w, byte r, byte g, byte b, byte a);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_dialog(ref WebView w, DialogType dlgtype, DialogFlags flags,
            [MarshalAs(UnmanagedType.LPStr)] string title,
            [MarshalAs(UnmanagedType.LPStr)] string arg,
            [MarshalAs(UnmanagedType.LPStr)] string result,
            UIntPtr resultsz);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_dispatch(ref WebView w,
            [MarshalAs(UnmanagedType.FunctionPtr)] WebViewDispatch fn,
            IntPtr arg);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_terminate(ref WebView w);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_exit(ref WebView w);

        public override int Simple(string title, string url, int width, int height, bool resizable)
        {
            return webview(title, url, width, height, resizable ? 1 : 0);
        }

        public override int Init(ref WebView webView)
        {
            return webview_init(ref webView);
        }

        public override int Loop(ref WebView webView, int blocking)
        {
            return webview_loop(ref webView, blocking);
        }

        public override int Eval(ref WebView webView, string js)
        {
            return webview_eval(ref webView, js);
        }

        public override int InjectCss(ref WebView webView, string css)
        {
            return webview_inject_css(ref webView, css);
        }

        public override void SetTitle(ref WebView webView, string title)
        {
            webview_set_title(ref webView, title);
        }

        public override void SetFullscreen(ref WebView webView, int fullscreen)
        {
            webview_set_fullscreen(ref webView, fullscreen);
        }

        public override void SetColor(ref WebView webView, byte r, byte g, byte b, byte a)
        {
            webview_set_color(ref webView, r, g, b, a);
        }

        public override void Dialog(ref WebView webView, DialogType dialogType, DialogFlags flags, string title,
            string arg, string result, int resultSize)
        {
            webview_dialog(ref webView, dialogType, flags, title, arg, result, (UIntPtr)resultSize);
        }

        public override void Dispatch(ref WebView webView, WebViewDispatch callback, IntPtr arg)
        {
            webview_dispatch(ref webView, callback, arg);
        }

        public override void Terminate(ref WebView webView)
        {
            webview_terminate(ref webView);
        }

        public override void Exit(ref WebView webView)
        {
            webview_exit(ref webView);
        }
    }

    internal class WebViewLibraryLinux : WebViewLibrary
    {
        private const string LibraryName = "webview";

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview(
            [MarshalAs(UnmanagedType.LPStr)] string title,
            [MarshalAs(UnmanagedType.LPStr)] string url,
            int width, int height, int resizable);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_init(ref WebView w);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_loop(ref WebView w, int blocking);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_eval(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string js);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int webview_inject_css(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string css);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_set_title(ref WebView w, [MarshalAs(UnmanagedType.LPStr)] string title);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_set_fullscreen(ref WebView w, int fullscreen);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_set_color(ref WebView w, byte r, byte g, byte b, byte a);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_dialog(ref WebView w, DialogType dlgtype, DialogFlags flags,
            [MarshalAs(UnmanagedType.LPStr)] string title,
            [MarshalAs(UnmanagedType.LPStr)] string arg,
            [MarshalAs(UnmanagedType.LPStr)] string result,
            UIntPtr resultsz);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_dispatch(ref WebView w,
            [MarshalAs(UnmanagedType.FunctionPtr)] WebViewDispatch fn,
            IntPtr arg);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_terminate(ref WebView w);

        [DllImport(LibraryName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void webview_exit(ref WebView w);

        public override int Simple(string title, string url, int width, int height, bool resizable)
        {
            return webview(title, url, width, height, resizable ? 1 : 0);
        }

        public override int Init(ref WebView webView)
        {
            return webview_init(ref webView);
        }

        public override int Loop(ref WebView webView, int blocking)
        {
            return webview_loop(ref webView, blocking);
        }

        public override int Eval(ref WebView webView, string js)
        {
            return webview_eval(ref webView, js);
        }

        public override int InjectCss(ref WebView webView, string css)
        {
            return webview_inject_css(ref webView, css);
        }

        public override void SetTitle(ref WebView webView, string title)
        {
            webview_set_title(ref webView, title);
        }

        public override void SetFullscreen(ref WebView webView, int fullscreen)
        {
            webview_set_fullscreen(ref webView, fullscreen);
        }

        public override void SetColor(ref WebView webView, byte r, byte g, byte b, byte a)
        {
            webview_set_color(ref webView, r, g, b, a);
        }

        public override void Dialog(ref WebView webView, DialogType dialogType, DialogFlags flags, string title,
            string arg, string result, int resultSize)
        {
            webview_dialog(ref webView, dialogType, flags, title, arg, result, (UIntPtr)resultSize);
        }

        public override void Dispatch(ref WebView webView, WebViewDispatch callback, IntPtr arg)
        {
            webview_dispatch(ref webView, callback, arg);
        }

        public override void Terminate(ref WebView webView)
        {
            webview_terminate(ref webView);
        }

        public override void Exit(ref WebView webView)
        {
            webview_exit(ref webView);
        }
    }
}
