﻿namespace WebView.Native
{
    public enum DialogType
    {
        Open = 0,
        Save = 1,
        Alert = 2
    }
}