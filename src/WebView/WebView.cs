﻿using System;
using WebView.Native;

namespace WebView
{
    public class WebView
    {
        private readonly Action<WebView, string> _externalCallback;
        private readonly WebViewExternalInvokeCallback _externalCallbackNative;
        private Native.WebView _webView;
        private volatile bool _shouldExit;

        public Native.WebView NativeWebView => _webView;

        public WebView(string title, string uri, (int width, int height) size, bool resizable,
            bool debug, Action<WebView, string> externalCallback)
        {
            _externalCallback = externalCallback;
            _externalCallbackNative = ExternalCallbackNative;
            _webView = new Native.WebView
            {
                Url = uri,
                Title = title,
                Width = size.width,
                Height = size.height,
                Resizable = resizable ? 1 : 0,
                Debug = debug ? 1 : 0,
                ExternalInvoke = _externalCallbackNative
            };
        }

        public void Run()
        {
            if (WebViewLibrary.Instance.Init(ref _webView) != 0)
                throw new Exception("Unable to initialize webview");

            while (WebViewLibrary.Instance.Loop(ref _webView, 1) == 0 && !_shouldExit)
            {
            }

            WebViewLibrary.Instance.Exit(ref _webView);
        }

        public bool Eval(string js)
        {
            return WebViewLibrary.Instance.Eval(ref _webView, js) == 0;
        }

        public void Dispatch(Action callback)
        {
            WebViewLibrary.Instance.Dispatch(ref _webView, (ref Native.WebView w, IntPtr arg) => callback(), IntPtr.Zero);
        }

        public void Exit()
        {
            _shouldExit = true;
            WebViewLibrary.Instance.Terminate(ref _webView);
        }

        private void ExternalCallbackNative(ref Native.WebView webView, string arg)
        {
            _externalCallback(this, arg);
        }
    }
}
