#!/bin/bash

webviewIn="webview/CMakeLists.txt"
webviewBkp="webview/CMakeLists.txt.bkp"

if [ ! -f "$webviewBkp" ]
then
    cp $webviewIn $webviewBkp
fi

sed 's/ole32 comctl32 oleaut32 uuid/-lole32 -lcomctl32 -loleaut32 -luuid/g' $webviewBkp > $webviewIn
echo "if(WIN32)
    set(CMAKE_EXE_LINKER_FLAGS " -static")
    target_link_libraries(webview -static-libgcc -static-libstdc++)
endif()" >> $webviewIn

rm -rf linux win64 win86
mkdir linux win64 win86

# Build linux
cd linux
cmake -DCMAKE_BUILD_TYPE=Release ..
make
cp webview/webview.so ..
cd ..

# Build win64
cd win64
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64.cmake ..
make
cp webview/webview_x64.dll ..
cd ..

# Build win86
cd win86
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win86.cmake ..
make
cp webview/webview_x86.dll ..
cd ..
